// Get all the keys from document
var keyPress = document.querySelectorAll('#calculator a');
var operators = ['+', '-', '*', '/'];
var decimalAdded = false;

// Add onclick event to all the keys and perform operations
for(var i = 0; i < keyPress.length; i++) {
	keyPress[i].onclick = function(e) {
		// Get the input and key values
		var screen = document.querySelector('.total');
		var screenValue = screen.innerHTML;
		var key = this.innerHTML;

		//if if Clear is pressed
		if(key == 'C')
		{
			screen.innerHTML = "";
			decimalAdded = false;
		}

		//If equal sign is entered
		else if(key == '=')
		{
				var res = screenValue;
				var finalRes = res[res.length - 1];

				if(operators.indexOf(finalRes) > -1 || finalRes == '.' || finalRes[finalRes.length - 1] == '-')
				{
					res = res.replace(/.$/, '');
				}

				if(res)
				{
					screen.innerHTML = eval(res);
				}
				decimalAdded = false;
		}

		//If operator is entered
		else if(operators.indexOf(key) > -1)
		{
			var finalRes = screenValue[screenValue.length - 1];

			if(screenValue != '' && operators.indexOf(res) == -1)
			{
				screen.innerHTML += key;
			}

			else if (screenValue == '' && key == '-')
			{
					screen.innerHTML += key;
			}
			// check if there is an operator entered, and then the user tries to enter
			// a second operator after that one and replace the previous operator
			// with the current operator entered by user
			// NOTE: Doesn't allow two minus operator entered one after another if
			// the screenValue is empty.
			if(operators.indexOf(finalRes) > -1 && screenValue.length > -1)
			{
					screen.innerHTML += key;
					screen.innerHTML = screenValue.replace(/.$/, key);
			}

			decimalAdded = false;
		}

		//if decimal point is entered
		else if(key == '.')
		{
			if(!decimalAdded)
			{
				screen.innerHTML += key;
				decimalAdded = true;
			}
		}
		//number is pressed
		else
		{
			if(operators.indexOf(finalRes) > -1 && screenValue.length > 1 && screenValue != '')
			{
				if(key == (operators.indexOf(finalRes) -1))
				{
					screen.innerHTML = screenValue.replace(/.$/, key);
				}
			}
			else
			{
				screen.innerHTML += key;
			}
		}

	e.preventDefault();
	}
}
